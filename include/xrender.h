#pragma once

namespace xr {

	class XRender;
	class WindowEvent;

	XRender* createXRender(int width, int height, bool fullScreen, int nativeWindowHandle = 0);
	void destroy(XRender*);

	struct WINDOW_CREATE_PARAMS {
		bool createWindow = true;
		int width;
		int height;
		bool fullscreen;
		int nativeWindow = 0;

	};

	class XRender {
	public:
		XRender() {}
		XRender(WINDOW_CREATE_PARAMS);
		virtual ~XRender() {};

	public:
		virtual bool pollMessage(WindowEvent*) = 0;
		virtual void setClearColor(float r, float g, float b, float a) = 0;
		virtual void clear() = 0;
		virtual void present() = 0;

	protected:
		virtual void initPlatform() = 0;
	
	};

	enum class WindowEventType {
		CLOSE, 
		KEY_DOWN,
		KEY_UP,
	};

	struct WindowEvent {
		WindowEventType type;
	};
}


