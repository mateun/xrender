#include "../include/xrender.h"

#ifdef WIN32
#include "../src/win32/xrender_win32.h"
#include <gl/gl.h>
#elif APPLE
#elif UNIX
#endif


int main(int argc, char** args) 
{
	xr::XRender* xr = xr::createXRender(800, 600, false);

	xr->setClearColor(1, 0, 0, 1);

	xr::WindowEvent we;


    // temp test
    glOrtho(0, 800, 0, 600, 0.01, 200);
    glViewport(0, 0, 800, 600);
    // end temp test


	bool run = true;
	while (run) {
		xr->pollMessage(&we);
		if (we.type == xr::WindowEventType::CLOSE) {
			run = false;
		}
		xr->clear();


		// temp test code
		glBegin(GL_TRIANGLES);
        glVertex3f(0, 0, -2);
        glVertex3f(800, 0, -2);
        glVertex3f(0, 600, -2);
        glEnd();


		// end temp test code

		xr->present();
	}
	
	xr::destroy(xr);
	return 0;


}
