#include "xrender_win32.h"
#include "xrender_win32.h"
#include "xrender_win32.h"
#include <Windows.h>
#include <gl/gl.h>
//#define WGL_WGLEXT_PROTOTYPES
#include "wglext.h"
#include <stdio.h>

#include "../../include/xrender.h"
#include "xrender_win32.h"


#pragma comment(lib, "opengl32.lib")

static HGLRC ourOpenGLRenderingContext = 0;

void initGL(HWND hwnd) {
    PIXELFORMATDESCRIPTOR pfd =
    {
        sizeof(PIXELFORMATDESCRIPTOR),
        1,
        PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,    //Flags
        PFD_TYPE_RGBA,        // The kind of framebuffer. RGBA or palette.
        32,                   // Colordepth of the framebuffer.
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0, 0, 0, 0,
        24,                   // Number of bits for the depthbuffer
        8,                    // Number of bits for the stencilbuffer
        0,                    // Number of Aux buffers in the framebuffer.
        PFD_MAIN_PLANE,
        0,
        0, 0, 0
    };

    HDC ourWindowHandleToDeviceContext = GetDC(hwnd);
    int  letWindowsChooseThisPixelFormat;
    letWindowsChooseThisPixelFormat = ChoosePixelFormat(ourWindowHandleToDeviceContext, &pfd);
    SetPixelFormat(ourWindowHandleToDeviceContext, letWindowsChooseThisPixelFormat, &pfd);

    ourOpenGLRenderingContext = wglCreateContext(ourWindowHandleToDeviceContext);
    wglMakeCurrent(ourWindowHandleToDeviceContext, ourOpenGLRenderingContext);

    // In case we select to use modern opengl (>= 3.2)
    // we need to define the macro below to use the attribute
    // based OpenGL context creation.
    // This is MANDATORY if we want to use RenderDoc!!
#ifdef MODERN_GL

    PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB = nullptr;
    wglCreateContextAttribsARB = reinterpret_cast<PFNWGLCREATECONTEXTATTRIBSARBPROC>(wglGetProcAddress("wglCreateContextAttribsARB"));
    if (wglCreateContextAttribsARB == nullptr) {
        // TODO
        // we might just as well accept
        // that we can not create a modern context,
        // which should actually
        // be ok for our purpose,
        // which is for now just software-rendered 3D.
        exit(1);
    } else {
        const int major_min = 4, minor_min = 5;
        int  contextAttribs[] = {
                WGL_CONTEXT_MAJOR_VERSION_ARB, major_min,
                WGL_CONTEXT_MINOR_VERSION_ARB, minor_min,
                WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
                0
        };

        HGLRC rc = wglCreateContextAttribsARB(ourWindowHandleToDeviceContext, 0, contextAttribs);
        if (rc == nullptr) {
            exit(1);
        } else {

            wglMakeCurrent(NULL,NULL);
            wglDeleteContext(ourOpenGLRenderingContext);
            wglMakeCurrent(ourWindowHandleToDeviceContext, rc);

        }

    }
#endif

    bool vsyncOK = ((BOOL(WINAPI*)(int))wglGetProcAddress("wglSwapIntervalEXT"))(1);
    if (!vsyncOK) {
        exit(1);
    }

    //MessageBoxA(0, (char*)glGetString(GL_VERSION), "OPENGL VERSION", 0);
    printf("GL Version %s\n", (char*)glGetString(GL_VERSION));
}


xr::XRenderWin32::XRenderWin32(WINDOW_CREATE_PARAMS wincreateParams) : _winCreateParams(wincreateParams)
{
    initPlatform();
}

xr::XRenderWin32::~XRenderWin32()
{
    wglDeleteContext(ourOpenGLRenderingContext);
}

static LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{

    HDC hdc;
    RECT rect;
    TEXTMETRIC tm;
    PAINTSTRUCT ps;

    switch (message) {

    case WM_CREATE:
    {
        initGL(hwnd);
        break;

    }
   

    case WM_CLOSE:
        /* if (MessageBox(NULL, "Are you sure you want to quit?",
                        "Confirmation", MB_ICONQUESTION | MB_YESNO) == IDYES)*/
        DestroyWindow(hwnd);
        break;

    case WM_DESTROY:
        PostQuitMessage(0);
        break;

    case WM_PAINT: {

        hdc = BeginPaint(hwnd, &ps);
        EndPaint(hwnd, &ps);

        break;
    }
    default:
        return DefWindowProc(hwnd, message, wParam, lParam);
    }
    return 0;
}



void xr::XRenderWin32::initPlatform()
{
	
    if (_winCreateParams.createWindow) {
        HINSTANCE hInstance = GetModuleHandle(nullptr);

        const char g_szClassName[] = "bonafideideasWindowClass";
        WNDCLASSEX wc;

        wc.lpszMenuName = NULL;
        wc.hInstance = hInstance;
        wc.lpszClassName = g_szClassName;
        wc.cbSize = sizeof(WNDCLASSEX);
        wc.cbClsExtra = wc.cbWndExtra = 0;
        wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
        wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
        wc.hCursor = LoadCursor(NULL, IDC_ARROW);
        wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
        wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
        wc.lpfnWndProc = WndProc;

        if (!RegisterClassEx(&wc)) {
            MessageBox(NULL, "Window Registration Failed", "Error", MB_ICONEXCLAMATION | MB_OK);
            exit(1);
        }

        RECT corrRect = { 0, 0, _winCreateParams.width, _winCreateParams.height};

        bool ok0 = AdjustWindowRect(&corrRect, WS_OVERLAPPEDWINDOW, false);

        /*bool ok =  AdjustWindowRectEx(
        &corrRect,
        WS_POPUPWINDOW | WS_CAPTION,
        true,
        WS_EX_CLIENTEDGE
        );
         */


        HWND hwnd = CreateWindow(
            g_szClassName,
            "XRender WIN32 window",
            WS_OVERLAPPEDWINDOW,
            400, 300, corrRect.right - corrRect.left, corrRect.bottom - corrRect.top,
            NULL, NULL, hInstance, NULL);

        if (hwnd == NULL) {
            MessageBox(NULL, "Window Creation Failed", "Error", MB_ICONEXCLAMATION | MB_OK);
            return;
        }

        ShowWindow(hwnd, SW_NORMAL);
        UpdateWindow(hwnd);

        _windowHDC = GetDC(hwnd);

       /* MSG msg;
        while (GetMessage(&msg, nullptr, 0, 0) > 0)
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }*/
    } 
    else  
    {
        initGL((HWND)(INT64)_winCreateParams.nativeWindow);
    }


}

bool xr::XRenderWin32::pollMessage(WindowEvent* ev)
{
    bool gotMessage = false;
    MSG msg;
    if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE) > 0) {
        gotMessage = true;
        TranslateMessage(&msg);
        DispatchMessage(&msg);
        switch (msg.message) {
        case WM_QUIT:
        case WM_CLOSE: 
        case WM_DESTROY:
        {
            ev->type = WindowEventType::CLOSE;

        }
        }
    }

    return gotMessage;

}

void xr::XRenderWin32::setClearColor(float r, float g, float b, float a)
{
    glClearColor(r, g, b, a);
}

void xr::XRenderWin32::clear()
{
    glClear(GL_COLOR_BUFFER_BIT);
}

void xr::XRenderWin32::present()
{
    //wglSwapLayerBuffers(_windowHDC, WGL_SWAP_MAIN_PLANE );
    SwapBuffers(_windowHDC);
}

