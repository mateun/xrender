#pragma once
#include "../../include/xrender.h"
#include <Windows.h>



namespace xr {

	

	class XRenderWin32 : public XRender {

	public:
		XRenderWin32(WINDOW_CREATE_PARAMS);
		~XRenderWin32();
		
		
	protected:
		void initPlatform() override;
		bool pollMessage(WindowEvent*) override;
		void setClearColor(float r, float g, float b, float a) override;
		void clear() override;
		void present() override;


	private:
		WINDOW_CREATE_PARAMS _winCreateParams;
		HDC _windowHDC;
	};
}