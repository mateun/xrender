#include "..\..\include\xrender.h"
#include "..\..\include\xrender.h"
#include "..\..\include\xrender.h"
#include "..\..\include\xrender.h"
#include "..\..\include\xrender.h"
#include "../../include/xrender.h"
#include <stdio.h>
#ifdef WIN32
#include "../win32/xrender_win32.h"
#endif

xr::XRender* xr::createXRender(int width, int height, bool fullScreen, int nativeWindowHandle)
{

	xr::WINDOW_CREATE_PARAMS wcc;
	wcc.createWindow = nativeWindowHandle == 0;
	if (!wcc.createWindow) {
		wcc.nativeWindow = nativeWindowHandle;
	}
	wcc.width = width;
	wcc.height = height;
	wcc.fullscreen = fullScreen;

#ifdef WIN32 
	
	return new xr::XRenderWin32(wcc);
#endif
	return nullptr;
}



void xr::destroy(XRender* xrender)
{
	if (xrender) {
		delete(xrender);
	}
}

xr::XRender::XRender(WINDOW_CREATE_PARAMS)
{

}

